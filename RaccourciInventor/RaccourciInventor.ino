
// Embedded software to control the position of a control panel
// using servomotor
// Autor: Adrien Jouve
// Date: April 14th 2019


#include <Servo.h>

// Servormotor object instentiation
Servo rotMotor;
Servo DropMotor;


// ServoMotor's pin control
int pinRotMotor(9); // Place ici le numéro de la pin de control du servo moteur
int pinDropMotor(10);  //chasse tirrette : chasser juste avant de descente mais faire un calcule du temps de montée et de descente pour savoir quand il faut chasser (uniquement dans la zone 4 à 5 sec)
// montée 5 sec uniquement lorsqu'on démarre sur le fin de course

// Up button pin
int up(4);      // Place ici le numéro de la pin du bouton MONTEE

// Down button pin
int down(2);      // Place ici le numéro de la pin du bouton DESCENTE

// Stop button pin
int stop(3);      // Place ici le numéro de la pin du bouton STOP

// Fin de course basse
int endDown(5);    // Place ici le numéro de la pin du CAPTEUR DE FIN DE COURSE BAS

// servomotor parameters
int stopServo(1495);   // Valeur pour stopper le servoMoteur
int upServo(120);    // Valeur pour monter
int downServo(60);   // Valeur pour Descendre
int minValDropServo(0);     // Valeur de début du servo motor de chasse tirette
int maxValDropServo(58);   // Valeur de fin du servo motor de chasse tirette (c'est ici pour inverser le sens du servo moteur de chasse tirette)

// time to push
int holdButtonTime(1200); // La durée d'appui (en ms) sur le button pour passer en mouvement maintenue
int upDelay(4910);      // Le temps de montée (en ms)

// Dropper activation time
unsigned long dropperActTime(4000);  // in ms
unsigned long beginDropTime(1000);   // in ms => Le temps durant lequel ion fait fonctionner le drop motor avant de démarrer la descente

// Global variable
unsigned long time(0);
bool holdButton(false);
unsigned long upTimeCounter;


// Check the push and hold button
// return true if we move in press and hold mode, aven false
bool holdButtonManagement(int preButton) {
  if( ((millis() - time) >= holdButtonTime) && ((preButton == 1 && digitalRead(up) == HIGH) || (preButton == 2 && digitalRead(down) == HIGH) ) )
    return true;

  return false;
}

// Check if the stop is requested when we are in mode press and hold
bool stopRequested() {
  if( digitalRead(stop) == HIGH ) {
    return true;
  }

  return false;
}


// Setup function to initilized the program
void setup() {

  // Initialize the servomotor
  rotMotor.attach(pinRotMotor);
  rotMotor.writeMicroseconds(stopServo);

  DropMotor.attach(pinDropMotor);
  DropMotor.write(minValDropServo);
  
  // Initialize buttons
  pinMode(up, INPUT);
  pinMode(down, INPUT);
  pinMode(stop, INPUT);

  // reset variable
  time = 0;
  upTimeCounter = 0;

  Serial.begin(9600);
  
}


// The main loop
void loop() {

  int prevButtonCheckVal;
  int servoAction;

  // Get the current time
  unsigned long timeStartServo = millis();
  time = millis();

  if( digitalRead(up) == HIGH && digitalRead(down) == LOW ) {   // Push the up button

    // Initialize the prevButtonCheckVal to the righ checker
    prevButtonCheckVal = 1;
    servoAction = upServo;

    Serial.println("in loop up");
    Serial.println(!holdButtonManagement(prevButtonCheckVal));
    Serial.println(((millis() - timeStartServo) + upTimeCounter) < upDelay);
    Serial.println(((millis() - timeStartServo) + upTimeCounter));
    Serial.println(((millis() - timeStartServo) >= upDelay ));
    Serial.println(upTimeCounter);
    while ( stopRequested() == false                          // temps qu'il n'y a pas de demande de STOP
      && !holdButtonManagement(prevButtonCheckVal)            // ET qu'il n'y a pas d'appuis long sur le button
      && ( ((millis() - timeStartServo) + upTimeCounter) < upDelay )
      && !( ((millis() - timeStartServo) >= upDelay )) ) {    // ET que si le moteur est en train de monter il ne faut pas le faire monté plus de 5 sec
      
      rotMotor.write(upServo);
      delay(100);
    }

    upTimeCounter += millis() - timeStartServo;
    Serial.println(upTimeCounter);
    
  } else if( digitalRead(down) == HIGH && digitalRead(up) == LOW ) {   // Push the down button

    bool isDropRequest( false );
    // Initialize the prevButtonCheckVal to the righ checker
    prevButtonCheckVal = 2;
    servoAction = downServo;

    // Support Remove
    if ( upTimeCounter >= dropperActTime ) {
      isDropRequest = true;
    }
    
    // Down
    Serial.println("in loop down");
    Serial.println(upTimeCounter);
    unsigned long currentDownTime = millis();
    while ( stopRequested() == false                          // temps qu'il n'y a pas de demande de STOP
      && !holdButtonManagement(prevButtonCheckVal)            // ET qu'il n'y a pas d'appuis long sur le button
      && !( digitalRead(endDown) == HIGH ) ) {                // ET que si le moteur est en train de descendre il faut le faire descendre jusqu'au capteur de fin de course bas

      // Chasse tirette en 2 temps
      if( isDropRequest && ((millis() - currentDownTime) <= beginDropTime/2)) {
        DropMotor.write(maxValDropServo/1.8);
      }else if (isDropRequest && ((millis() - currentDownTime) >= beginDropTime/2)){
        DropMotor.write(maxValDropServo);
      }

      rotMotor.write(downServo);
      
      if( isDropRequest && ((millis() - currentDownTime) >= beginDropTime) ) {
        isDropRequest = false;
        timeStartServo = millis();
        DropMotor.write(minValDropServo);
      }
    }
    //DropMotor.write(minValDropServo);
    // Up time counter reset
    delay(2);
    if (digitalRead(endDown) == HIGH) {
      upTimeCounter = 0;
    }else {
      if ((millis() - timeStartServo) > upTimeCounter) {
        upTimeCounter = 0;
      }else {
        upTimeCounter -= millis() - timeStartServo;
      }
    }
    Serial.println(upTimeCounter);
    DropMotor.write(minValDropServo);
  }

  // manage HoldButton
  timeStartServo = millis();
  if ( holdButtonManagement(prevButtonCheckVal) ) {
    while ( (digitalRead(up) == HIGH && ( (millis() - timeStartServo) + upTimeCounter < upDelay )) || (digitalRead(down) == HIGH && !(digitalRead(endDown) == HIGH) ) ) {
      rotMotor.write(servoAction);
      delay(100);
    }
    // Up time counter management
    if (servoAction == upServo && ((millis() - timeStartServo) + upTimeCounter < upDelay ) ) {
        upTimeCounter += millis() - timeStartServo;
    }
    // in Down movement so manage time counter
    if (servoAction == downServo && !(digitalRead(endDown) == HIGH) ) {
      if ((millis() - timeStartServo) > upTimeCounter) {
        upTimeCounter = 0;
      }else {
        upTimeCounter -= millis() - timeStartServo;
      }
    }
    
    // in all cases if arrived to the end of down course = reset the counter
    if ( digitalRead(endDown) == HIGH ) {
      upTimeCounter = 0;
    }
  }
    
  rotMotor.writeMicroseconds(stopServo);
  // reset variable
  time = 0;
  
  // Sleep a moment to save power (in ms)
  delay(100);
}
